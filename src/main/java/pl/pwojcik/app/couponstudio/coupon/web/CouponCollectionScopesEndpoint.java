package pl.pwojcik.app.couponstudio.coupon.web;

import io.reactivex.Single;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.service.CouponCollectionScopesService;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/coupons/scopes", consumes = APPLICATION_JSON_VALUE)
public class CouponCollectionScopesEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(CouponCollectionScopesEndpoint.class);
    private static final String TENANT_HEADER_NAME = "cp-tenant";

    @Autowired
    private CouponCollectionScopesService scopeService;

    @Value("${coupon.endpoint.timeout}")
    private long timeout;

    @PatchMapping
    public DeferredResult<ResponseEntity<Void>> addScopesToAllCoupons(
            @RequestBody final Collection<CouponScope> scopes,//
            @RequestHeader(TENANT_HEADER_NAME) final String tenant) {//
        final String logPattern = "Successfully added scopes for all coupons for tenant {}: {}";
        return processScopeOperationForTenant(//
                (couponScopes, couponTenant) -> //
                        scopeService.addScopesToAllCoupons(couponScopes, couponTenant),
                scopes, tenant, logPattern);
    }

    @DeleteMapping
    public DeferredResult<ResponseEntity<Void>> removeScopesFromAllCoupons(
            @RequestBody final Collection<String> locationIds,//
            @RequestHeader(TENANT_HEADER_NAME) final String tenant) {//
        final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
        scopeService.removeScopesFromAllCoupons(locationIds, tenant)//
                .map(avoid -> ResponseEntity.ok().<Void>build())//
                .doOnSuccess(avoid ->
                        LOG.debug("Successfully removed scopes from locations {} for all coupons of tenant {}.",
                                String.join(",", locationIds), tenant)
                )//
                .subscribe(deferred::setResult, deferred::setErrorResult);
        return deferred;
    }

    private DeferredResult<ResponseEntity<Void>> processScopeOperationForTenant(
            final BiFunction<Collection<CouponScope>, String, Single<Boolean>> serviceOperation,
            final Collection<CouponScope> scopes, final String tenant, final String logPattern) {

        final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
        serviceOperation.apply(scopes, tenant)//
                .map(avoid -> ResponseEntity.ok().<Void>build())//
                .doOnSuccess(avoid -> {
                    final String scopesList = scopes.stream()//
                            .map(CouponScope::toString)//
                            .collect(Collectors.joining(","));
                    LOG.debug(logPattern, tenant, scopesList);
                })//
                .subscribe(deferred::setResult, deferred::setErrorResult);
        return deferred;
    }
}
