package pl.pwojcik.app.couponstudio.coupon.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponCollectionScopesRepository;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

@Component
public class CouponCollectionScopesService
{
	@Autowired
	private CouponValidator validator;

	@Autowired
	private CouponCollectionScopesRepository repository;

	public Single<Boolean> addScopesToAllCoupons(final Collection<CouponScope> couponScopes,//
			final String tenant)
	{
		return repository.addScopesToTenantCoupons(couponScopes, tenant);
	}

	public Single<Boolean> removeScopesFromAllCoupons(final Collection<String> locationIds,//
			final String tenant)
	{
		return validator.validateScopeIds(locationIds)//
				.flatMap(avoid -> repository.removeScopesInTenantCoupons(locationIds, tenant));
	}
}
