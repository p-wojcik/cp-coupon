package pl.pwojcik.app.couponstudio.coupon.service;

import static java.util.Objects.isNull;

import java.util.Collections;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponRepository;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

@Component
public class CouponBasicOperationsService {

    private static final Logger LOG = LoggerFactory.getLogger(CouponBasicOperationsService.class);

    @Autowired
    private CouponValidator validator;

    @Autowired
    private CouponRepository repository;

    public Single<String> create(final String tenant, final CreateCouponEvent createCouponEvent, final String urlInfo) {
        return validator.validateCoupon(createCouponEvent)//
                .doOnSuccess(item -> LOG.debug("CreateCouponEvent has been validated successfully: {}.", createCouponEvent))//
                .map(this::checkScopesData)//
                .map(this::generateCouponID)//
                .map(item -> generateCouponCode(tenant, item))//
				.flatMap(item -> repository.create(tenant, item)//
						.doOnSuccess(avoid ->//
								LOG.debug("CreateCouponEvent with ID {} has been saved in database.", item.getId()))//
						.map(avoid -> generateLocationHeaderURL(item, urlInfo)));
	}

    private String generateLocationHeaderURL(final CreateCouponEvent createCouponEvent, final String urlInfo) {
        return urlInfo + "/" + createCouponEvent.getId().toString();
    }

	private CreateCouponEvent checkScopesData(final CreateCouponEvent item)
	{
		if (isNull(item.getScopes()))
		{
			return new CreateCouponEvent(//
					item.getCodeSecret(), item.getName(), item.getDescription(), Collections.emptySet(),//
					item.isMediaAvailable(), item.isActive());
		}
		return item;
	}

    private CreateCouponEvent generateCouponID(final CreateCouponEvent item) {
        final UUID id = UUID.randomUUID();
        LOG.debug("Updated coupon {} with new ID: {}.", item, id);
        item.setId(id);
        return item;
    }

    private CreateCouponEvent generateCouponCode(final String tenant, final CreateCouponEvent item) {
        final String code = EncodingUtility.encodeForTenant(tenant, item.getCodeSecret(), item.getId().toString());
        LOG.debug("Updated coupon {} with new code: {}.", item, code);
        item.setCode(code);
        return item;
    }

    public Single<CouponEvent> getById(final String tenant, final String uuid) {
        return validator.validateCouponId(uuid)//
                .doOnSuccess(id -> LOG.debug("Getting coupon for tenant: {}, ID: {}.", tenant, id))//
                .flatMap(id -> repository.getById(tenant, id));
    }

    public Single<CouponEvent> getByCode(final String code) {
        final Pair<String, String> tenantAndCode = EncodingUtility.decodeTenantAndAccessCode(code);
        LOG.debug("Successfully decoded {} for tenant {}.", code, tenantAndCode.getLeft());
        return repository.getByCode(tenantAndCode.getLeft(), code);
    }

    public Single<Boolean> delete(final String tenant, final String id)
    {
        return repository.delete(tenant, id);
    }
}
