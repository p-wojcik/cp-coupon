package pl.pwojcik.app.couponstudio.coupon.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.CouponCollectionOperationService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/coupons", produces = APPLICATION_JSON_VALUE)
public class CouponCollectionOperationEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(CouponCollectionOperationEndpoint.class);
    private static final String TENANT_HEADER_NAME = "cp-tenant";

    @Autowired
    private CouponCollectionOperationService operationsService;

    @Value("${coupon.endpoint.timeout}")
    private long timeout;

    @GetMapping
    public DeferredResult<ResponseEntity<List<CouponEvent>>> getAllCoupons(
            @RequestHeader(TENANT_HEADER_NAME) final String tenant) {
        final DeferredResult<ResponseEntity<List<CouponEvent>>> deferred = new DeferredResult<>(timeout);
        operationsService.getAllCoupons(tenant)//
                .doOnSuccess(item -> LOG.debug("Successful GET for all coupons for tenant: {}", tenant))//
                .map(ResponseEntity::ok)//
                .subscribe(deferred::setResult, deferred::setErrorResult);
        return deferred;
    }
}
