package pl.pwojcik.app.couponstudio.coupon.exception;

import java.util.UUID;

/**
 * Exception indicating situation when scopes of coupon could not be modified.
 */
public final class CouponScopesNotModifiedException extends RuntimeException {

    private static final String ID_PATTERN = "Coupon scopes for coupon with ID %s could not be modified for tenant %s";

    public CouponScopesNotModifiedException(final String tenant, final UUID id) {
        super(String.format(ID_PATTERN, id.toString(), tenant));
    }
}
