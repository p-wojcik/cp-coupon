package pl.pwojcik.app.couponstudio.coupon.repository;

import static com.google.common.collect.ImmutableMap.of;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.mongodb.BasicDBObject;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotDeletedException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundForCodeException;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;

@Component
public class CouponRepository
{
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_FIELD = "_id";
	private static final String CODE_FIELD = "code";
	private static final String TENANT_FIELD = "tenant";

	@Autowired
	private MongoConnector connector;

	/**
	 * Method for persisting Coupon in database.
	 *
	 * @param tenant tenant
	 * @param createCouponEvent coupon details
	 * @return true, if object has been persisted, Single.error otherwise
	 */
	public Single<Boolean> create(final String tenant, final CreateCouponEvent createCouponEvent)
	{
		final Map<String, Object> couponAttributes = OBJECT_MAPPER.convertValue(createCouponEvent, Map.class);

		final Document document = new Document(couponAttributes);
		document.put(TENANT_FIELD, tenant);
		connector.getCollection().insertOne(document);
		return Single.just(true);
	}

	/**
	 * Method for fetching Coupon by ID from database
	 *
	 * @param id ID of Coupon
	 * @return Single with CouponEvent or Single.error in case of error
	 */
	public Single<CouponEvent> getById(final String tenant, final UUID id)
	{
		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id.toString(),//
				TENANT_FIELD, tenant//
		));
		final Document result = connector.getCollection().find(query).first();
		return Optional.ofNullable(result)//
				.map(rawData -> OBJECT_MAPPER.convertValue(rawData, CouponEvent.class))//
				.map(Single::just)//
				.orElse(Single.error(new CouponNotFoundException(id.toString(), tenant)));
	}

	/**
	 * Method for fetching Coupon by code from database
	 *
	 * @param tenant tenant
	 * @param code code of Coupon
	 * @return Single with CouponEvent or Single.error in case of error
	 */
	public Single<CouponEvent> getByCode(final String tenant, final String code)
	{
		final BasicDBObject query = new BasicDBObject(of(//
				CODE_FIELD, code,//
				TENANT_FIELD, tenant//
		));
		final Document result = connector.getCollection().find(query).first();
		return Optional.ofNullable(result)//
				.map(rawData -> OBJECT_MAPPER.convertValue(rawData, CouponEvent.class))//
				.map(Single::just)//
				.orElse(Single.error(new CouponNotFoundForCodeException(code)));
	}

	/**
	 * Method for deleting coupon from database.
	 *
	 * @param tenant tenant
	 * @param id ID of Coupon
	 * @return true if Coupon has been deleted, Single.error otherwise
	 */
	public Single<Boolean> delete(final String tenant, final String id)
	{
		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id,//
				TENANT_FIELD, tenant//
		));
		final long deletedCount = connector.getCollection().deleteOne(query).getDeletedCount();
		if (deletedCount == 1)
		{
			return Single.just(true);
		}
		return Single.error(new CouponNotDeletedException(id, tenant));
	}

	@VisibleForTesting
	void deleteAllDataFromBackingCollection()
	{
		connector.getCollection().deleteMany(new Document());
	}
}
