package pl.pwojcik.app.couponstudio.coupon.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.CouponBasicOperationsService;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/coupons", produces = APPLICATION_JSON_VALUE)
public class CouponBasicOperationsEndpoint
{

	private static final Logger LOG = LoggerFactory.getLogger(CouponBasicOperationsEndpoint.class);
	private static final String TENANT_HEADER_NAME = "cp-tenant";

	@Autowired
	private CouponBasicOperationsService operationsService;

	@Value("${coupon.endpoint.timeout}")
	private long timeout;

	@PostMapping(consumes = APPLICATION_JSON_VALUE)
	public DeferredResult<ResponseEntity<Void>> createCoupon(@RequestBody final CreateCouponEvent createCouponEvent,
			final HttpServletRequest request)
	{//
		final String urlInfo = request.getRequestURL().toString();
		final String tenant = request.getHeader(TENANT_HEADER_NAME);
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		operationsService.create(tenant, createCouponEvent, urlInfo)//
				.map(this::createSaveResponse)//
				.doOnSuccess(item -> LOG.debug("Successfully generated Location header for createCouponEvent " +//
						"with ID {} for tenant {}: {}", createCouponEvent.getId(), tenant, item.getHeaders().getLocation()))//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	private ResponseEntity<Void> createSaveResponse(final String url)
	{
		return ResponseEntity.created(URI.create(url)).build();
	}

	@GetMapping(path = "/{id}")
	public DeferredResult<ResponseEntity<CouponEvent>> getCouponById(@PathVariable("id") final String uuid,
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<CouponEvent>> deferred = new DeferredResult<>(timeout);
		operationsService.getById(tenant, uuid).doOnSuccess(
				item -> LOG.debug("Successful GET for coupon with ID {} for tenant: {}", uuid, tenant))//
				.map(ResponseEntity::ok)//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@GetMapping(path = "/code/{code}")
	public DeferredResult<ResponseEntity<CouponEvent>> getCouponByCode(@PathVariable("code") final String code)
	{
		final DeferredResult<ResponseEntity<CouponEvent>> deferred = new DeferredResult<>(timeout);
		operationsService.getByCode(code)//
				.doOnSuccess(item -> LOG.debug("Successful GET for coupon with code {}", code))//
				.map(ResponseEntity::ok)//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@DeleteMapping(path = "/{id}")
	public DeferredResult<ResponseEntity<Void>> deleteCouponById(//
			@PathVariable("id") final String id, @RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		operationsService.delete(tenant, id)//
				.doOnSuccess(item -> LOG.debug("Successful DELETE for coupon with id {} for tenant", id, tenant))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}
}