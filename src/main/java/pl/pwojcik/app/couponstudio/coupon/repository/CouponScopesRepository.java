package pl.pwojcik.app.couponstudio.coupon.repository;

import static com.google.common.collect.ImmutableMap.of;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponScopesNotModifiedException;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;

@Component
public class CouponScopesRepository
{
	private static final Logger LOG = LoggerFactory.getLogger(CouponScopesRepository.class);
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_FIELD = "_id";
	private static final String SCOPES_FIELD = "scopes";
	private static final String TENANT_FIELD = "tenant";
	private static final String LOCATION_ID_FIELD = "locationId";

	private static final String MONGO_EACH_OPERATION = "$each";
	private static final String MONGO_ADD_TO_SET_OPERATION = "$addToSet";
	private static final String MONGO_IN_OPERATION = "$in";
	private static final String MONGO_PULL_OPERATION = "$pull";

	@Autowired
	private MongoConnector connector;

	/**
	 * Method for adding scopes to locations (specified as parameter) from Coupon
	 *
	 * @param scopes collection of scopes where coupon availability should be added
	 * @param tenant tenant
	 * @param id ID of Coupon
	 * @return true if scopes have been added, Single.error otherwise
	 */
	public Single<Boolean> addScopesToCoupon(final Collection<CouponScope> scopes, //
			final String tenant, final UUID id)
	{
		final Object[] scopesArray = scopes.stream()//
				.map(scope -> OBJECT_MAPPER.convertValue(scope, Map.class))//
				.toArray();

		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id.toString(),//
				TENANT_FIELD, tenant//
		));
		final BasicDBObject update = new BasicDBObject(MONGO_ADD_TO_SET_OPERATION,//
				new BasicDBObject(SCOPES_FIELD,//
						new BasicDBObject(MONGO_EACH_OPERATION, scopesArray)//
				)//
		);
		final long matchedCount = connector.getCollection()//
				.updateOne(query, update)//
				.getModifiedCount();
		if (matchedCount == 1)
		{
			return Single.just(true);
		}
		LOG.warn("Expected to modify 1 entry instead of {} entries while adding scopes for coupon with id {} " +//
				"for tenant {}. ", matchedCount, id.toString(), tenant);
		return Single.error(new CouponScopesNotModifiedException(tenant, id));
	}

	/**
	 * Method for removing scopes from locations (with IDs specified as parameter) from Coupon
	 *
	 * @param locationIds collection of Location IDs where coupon availability should be removed
	 * @param tenant tenant
	 * @param id ID of Coupon
	 * @return true if scopes have been removed, Single.error otherwise
	 */
	public Single<Boolean> removeScopesInCoupon(final Collection<String> locationIds, //
			final String tenant, final UUID id)
	{
		final String[] locationIdsArray = locationIds.toArray(new String[]{});
		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id.toString(),//
				TENANT_FIELD, tenant//
		));
		final BasicDBObject update = new BasicDBObject(MONGO_PULL_OPERATION,//
				new BasicDBObject(SCOPES_FIELD,//
						new BasicDBObject(LOCATION_ID_FIELD,//
								new BasicDBObject(MONGO_IN_OPERATION, locationIdsArray)//
						)//
				)//
		);
		final long matchedCount = connector.getCollection()//
				.updateOne(query, update)//
				.getMatchedCount();
		if (matchedCount == 1)
		{
			return Single.just(true);
		}
		LOG.warn("Expected to modify 1 entry instead of {} entries while removing scopes for coupon with id {} " +//
				"for tenant {}.", matchedCount, id.toString(), tenant);
		return Single.error(new CouponScopesNotModifiedException(tenant, id));
	}
}
