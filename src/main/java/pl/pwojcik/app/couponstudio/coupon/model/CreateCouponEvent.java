package pl.pwojcik.app.couponstudio.coupon.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import pl.pwojcik.app.couponstudio.coupon.model.serialization.UUIDSerializer;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

/**
 * Class aggregating set of parameters required to create Coupon.
 */
public class CreateCouponEvent
{

	@JsonProperty(value = "_id", access = READ_ONLY)
	@JsonSerialize(using = UUIDSerializer.class)
	private UUID id;
	@JsonProperty(access = READ_ONLY)
	private String code;
	private String codeSecret;
	private final String name;
	private final String description;
	private final Set<CouponScope> scopes;
	private final boolean mediaAvailable;
	private final boolean active;

	@JsonCreator
	public CreateCouponEvent(//
			@JsonProperty(value = "codeSecret", access = WRITE_ONLY) final String codeSecret,//
			@JsonProperty("name") final String name,//
			@JsonProperty("description") final String description,//
			@JsonProperty("scopes") final Set<CouponScope> scopes,//
			@JsonProperty("mediaAvailable") final boolean mediaAvailable,//
			@JsonProperty("active") final boolean active)
	{
		this.codeSecret = codeSecret;
		this.name = name;
		this.description = description;
		this.scopes = scopes;
		this.mediaAvailable = mediaAvailable;
		this.active = active;
	}

	public void setId(final UUID id)
	{
		this.id = id;
	}

	public UUID getId()
	{
		return id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getCodeSecret()
	{
		return codeSecret;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public Set<CouponScope> getScopes()
	{
		return scopes;
	}

	public boolean isMediaAvailable()
	{
		return mediaAvailable;
	}

	public boolean isActive()
	{
		return active;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)//
				.append("id", id)//
				.append("code", code)//
				.append("codeSecret", codeSecret)//
				.append("name", name)//
				.append("description", description)//
				.append("scopes", scopes)//
				.append("mediaAvailable", mediaAvailable)//
				.append("active", active)//
				.build();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()//
				.append(id)//
				.append(code)//
				.append(codeSecret)//
				.append(name)//
				.append(description)//
				.append(scopes)//
				.append(mediaAvailable)//
				.append(active)//
				.build();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj == this)
		{
			return true;
		}
		if (Objects.isNull(obj) || !obj.getClass().equals(getClass()))
		{
			return false;
		}
		final CreateCouponEvent that = CreateCouponEvent.class.cast(obj);
		return new EqualsBuilder()//
				.append(that.id, id)//
				.append(that.code, code)//
				.append(that.codeSecret, codeSecret)//
				.append(that.name, name)//
				.append(that.description, description)//
				.append(that.scopes, scopes)//
				.append(that.mediaAvailable, mediaAvailable)//
				.append(that.active, active)//
				.build();
	}
}
