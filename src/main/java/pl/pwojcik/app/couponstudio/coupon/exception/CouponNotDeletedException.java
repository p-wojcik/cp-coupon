package pl.pwojcik.app.couponstudio.coupon.exception;

/**
 * Exception indicating that some errors occurred during deleting Coupon and some related data
 * may still exists in database.
 */
public final class CouponNotDeletedException extends RuntimeException {

    private static final String SINGLE_PATTERN = "Coupon with id %s could not be deleted for tenant %s. " +
            "Contact system administrator.";

    public CouponNotDeletedException(final String couponId, final String tenant) {
        super(String.format(SINGLE_PATTERN, couponId, tenant));
    }

}
