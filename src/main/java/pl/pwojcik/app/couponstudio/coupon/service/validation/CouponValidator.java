package pl.pwojcik.app.couponstudio.coupon.service.validation;

import io.reactivex.Single;
import org.springframework.stereotype.Component;
import pl.pwojcik.app.couponstudio.coupon.exception.InvalidCouponDetailsException;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;

import java.util.Collection;
import java.util.UUID;
import java.util.function.Function;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
public class CouponValidator {

    /**
     * Performs necessary validation on Coupon which is going to be persisted.
     *
     * @param createCouponEvent event to be persisted
     * @return validated event
     */
    public Single<CreateCouponEvent> validateCoupon(final CreateCouponEvent createCouponEvent) {
        return Single.just(createCouponEvent)//
                .flatMap(item -> validateAttribute(createCouponEvent, CreateCouponEvent::getCodeSecret, "Code secret"))//
                .flatMap(item -> validateAttribute(createCouponEvent, CreateCouponEvent::getName, "Name"))//
                .flatMap(item -> validateAttribute(createCouponEvent, CreateCouponEvent::getDescription, "Description"));
    }

    private Single<CreateCouponEvent> validateAttribute(final CreateCouponEvent createCouponEvent, final Function<CreateCouponEvent, String> attributeGetter,//
                                                        final String attributeName) {
        final String attributeValue = attributeGetter.apply(createCouponEvent);
        if (isBlank(attributeValue)) {
            final String message = String.format("%s cannot be empty or null.", attributeName);
            final InvalidCouponDetailsException exception = new InvalidCouponDetailsException(message);
            return Single.error(exception);
        }
        return Single.just(createCouponEvent);
    }

    /**
     * Validates ID which is needed to get Coupon.
     *
     * @param uuid potential ID of coupon
     * @return UUID
     */
    public Single<UUID> validateCouponId(final String uuid) {
        try {
            final UUID id = UUID.fromString(uuid);
            return Single.just(id);
        } catch (final IllegalArgumentException exception) {
            final String message = "Invalid coupon ID: " + uuid;
            final InvalidCouponDetailsException repacked = new InvalidCouponDetailsException(message);
            return Single.error(repacked);
        }
    }

    public Single<Collection<String>> validateScopeIds(final Collection<String> locationIds) {
        try {
            for (final String scopeId : locationIds) {
                UUID.fromString(scopeId);
            }
            return Single.just(locationIds);
        } catch (final IllegalArgumentException exception) {
            final String message = "Invalid scope found in collection: " + String.join(",", locationIds);
            final InvalidCouponDetailsException repacked = new InvalidCouponDetailsException(message);
            return Single.error(repacked);
        }

    }
}