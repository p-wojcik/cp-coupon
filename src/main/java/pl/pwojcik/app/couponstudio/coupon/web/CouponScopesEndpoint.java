package pl.pwojcik.app.couponstudio.coupon.web;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Collection;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.service.CouponScopesService;
import pl.pwojcik.app.couponstudio.coupon.util.TriFunction;

@RestController
@RequestMapping(value = "/coupons/{id}/scopes", consumes = APPLICATION_JSON_VALUE)
public class CouponScopesEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(CouponScopesEndpoint.class);
    private static final String TENANT_HEADER_NAME = "cp-tenant";

    @Autowired
    private CouponScopesService scopeService;

    @Value("${coupon.endpoint.timeout}")
    private long timeout;

    @PatchMapping
    public DeferredResult<ResponseEntity<Void>> addScopesForCoupon(
            @RequestBody final Collection<CouponScope> scopes,//
            @PathVariable("id") final String uuid,
            @RequestHeader(TENANT_HEADER_NAME) final String tenant) {//
        final String logPattern = "Successfully added scopes for coupon with ID {} for tenant {}: {}";
        return processScopeOperationForCoupon(//
                (couponScopes, couponTenant, couponId) -> //
                        scopeService.addScopesForCoupon(couponScopes, couponTenant, couponId),
                scopes, tenant, uuid, logPattern);
    }

    private DeferredResult<ResponseEntity<Void>> processScopeOperationForCoupon(
            final TriFunction<Collection<CouponScope>, String, String, Single<Boolean>> serviceOperation,
            final Collection<CouponScope> scopes, final String tenant, final String id,
            final String logPattern) {

        final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
        serviceOperation.apply(scopes, tenant, id)//
                .map(avoid -> ResponseEntity.ok().<Void>build())//
                .doOnSuccess(avoid -> {
                    final String scopesList = scopes.stream()//
                            .map(CouponScope::toString)//
                            .collect(Collectors.joining(","));
                    LOG.debug(logPattern, id, tenant, scopesList);
                })//
                .subscribe(deferred::setResult, deferred::setErrorResult);
        return deferred;
    }

    @DeleteMapping
    public DeferredResult<ResponseEntity<Void>> removeScopesForCoupon(
            @RequestBody final Collection<String> scopes,//
            @PathVariable("id") final String uuid,
            @RequestHeader(TENANT_HEADER_NAME) final String tenant) {//
        final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
        scopeService.removeScopesForCoupon(scopes, tenant, uuid)//
                .map(avoid -> ResponseEntity.ok().<Void>build())//
                .doOnSuccess(avoid ->
                        LOG.debug("Successfully removed scopes from locations {} for coupon with ID {} of tenant {}",
                                String.join(",", scopes), uuid, tenant)
                )//
                .subscribe(deferred::setResult, deferred::setErrorResult);
        return deferred;
    }


}