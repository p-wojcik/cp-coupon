package pl.pwojcik.app.couponstudio.coupon.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import pl.pwojcik.app.couponstudio.coupon.service.CouponActivityStatusService;

@RestController
@RequestMapping(value = "/coupons")
public class CouponActivityStatusEndpoint
{
	private static final Logger LOG = LoggerFactory.getLogger(CouponActivityStatusEndpoint.class);
	private static final String TENANT_HEADER_NAME = "cp-tenant";

	@Autowired
	private CouponActivityStatusService activityStatusService;

	@Value("${coupon.endpoint.timeout}")
	private long timeout;

	@PutMapping(path = "/{id}/active")
	public DeferredResult<ResponseEntity<Void>> enableCoupon(//
			@PathVariable("id") final String id,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		activityStatusService.enableCoupon(tenant, id)//
				.doOnSuccess(item -> LOG.debug("Coupon with id {} successfully enabled for tenant {}", id, tenant))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@DeleteMapping(path = "/{id}/active")
	public DeferredResult<ResponseEntity<Void>> disableCoupon(//
			@PathVariable("id") final String id,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		activityStatusService.disableCoupon(tenant, id)//
				.doOnSuccess(item -> LOG.debug("Coupon with id {} successfully disabled for tenant {}", id, tenant))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@PutMapping(path = "/active")
	public DeferredResult<ResponseEntity<Void>> enableAllCoupons(//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		activityStatusService.enableAllCoupons(tenant)//
				.doOnSuccess(item -> LOG.debug("Coupons successfully enabled for tenant {}", tenant))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@DeleteMapping(path = "/active")
	public DeferredResult<ResponseEntity<Void>> disableAllCoupons(//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);
		activityStatusService.disableAllCoupons(tenant)//
				.doOnSuccess(item -> LOG.debug("Coupons successfully disabled for tenant {}", tenant))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@PutMapping(path = "/{id}/active/location/{locationId}")
	public DeferredResult<ResponseEntity<Void>> enableCouponInSpecifiedLocation(//
			@PathVariable("id") final String id,//
			@PathVariable("locationId") final String locationId,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);

		activityStatusService.enableCouponInSpecifiedLocation(tenant, id, locationId)//
				.doOnSuccess(item -> LOG.debug("Coupon with id {} successfully enabled for tenant {} in locations {}",//
						id, tenant, locationId))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@DeleteMapping(path = "/{id}/active/location/{locationId}")
	public DeferredResult<ResponseEntity<Void>> disableCouponInSpecifiedLocation(//
			@PathVariable("id") final String id,//
			@PathVariable("locationId") final String locationId,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);

		activityStatusService.disableCouponInSpecifiedLocation(tenant, id, locationId)//
				.doOnSuccess(item -> LOG.debug("Coupon with id {} successfully disabled for tenant {} in location {}",//
						id, tenant, locationId))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@PutMapping(path = "/all/active/location/{locationId}")
	public DeferredResult<ResponseEntity<Void>> enableTenantCouponsInSpecifiedLocation(//
			@PathVariable("locationId") final String locationId,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);

		activityStatusService.enableTenantCouponsInSpecifiedLocation(tenant, locationId)//
				.doOnSuccess(item -> LOG.debug("Tenant coupons successfully enabled for tenant {} in location {}",//
						tenant, locationId))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}

	@DeleteMapping(path = "/all/active/location/{locationId}")
	public DeferredResult<ResponseEntity<Void>> disableTenantCouponsInSpecifiedLocation(//
			@PathVariable("locationId") final String locationId,//
			@RequestHeader(TENANT_HEADER_NAME) final String tenant)
	{
		final DeferredResult<ResponseEntity<Void>> deferred = new DeferredResult<>(timeout);

		activityStatusService.disableTenantCouponsInSpecifiedLocation(tenant, locationId)//
				.doOnSuccess(item -> LOG.debug("Tenant coupon successfully disabled for tenant {} in location {}",//
						tenant, locationId))//
				.map(avoid -> ResponseEntity.noContent().<Void>build())//
				.subscribe(deferred::setResult, deferred::setErrorResult);
		return deferred;
	}
}
