package pl.pwojcik.app.couponstudio.coupon.web;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.ImmutableMap;

import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotDeletedException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundForCodeException;
import pl.pwojcik.app.couponstudio.coupon.exception.InvalidCouponDetailsException;

@ControllerAdvice
public class ExceptionMapper
{

	private static final Logger LOG = LoggerFactory.getLogger(ExceptionMapper.class);

	@ExceptionHandler(InvalidCouponDetailsException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public String handleInvalidCouponDetailsException(final InvalidCouponDetailsException exception)
	{
		return buildErrorResponseJSON(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
	}

	@ExceptionHandler(CouponNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public String handleCouponNotFoundException(final CouponNotFoundException exception)
	{
		LOG.debug("{} (for tenant {})", exception.getMessage(), exception.getTenant());
		return buildErrorResponseJSON(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

	@ExceptionHandler(CouponNotFoundForCodeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public String handleCouponNotFoundForCodeException(final CouponNotFoundForCodeException exception)
	{
		LOG.warn(exception.getMessage());
		return buildErrorResponseJSON(HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

	@ExceptionHandler(CouponNotDeletedException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public String handleCouponNotDeletedException(final CouponNotDeletedException exception)
	{
		LOG.warn(exception.getMessage());
		return buildErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public String handleInternalErrors(final Exception exception)
	{
		LOG.error("Unhandled internal error", exception);
		return buildErrorResponseJSON(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage());
	}

	private String buildErrorResponseJSON(final int code, final String message)
	{
		final JSONObject errorResponse = new JSONObject(ImmutableMap.of(//
				"code", code,//
				"message", message//
		));
		return errorResponse.toString();
	}
}
