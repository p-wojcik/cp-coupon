package pl.pwojcik.app.couponstudio.coupon.exception;

/**
 * Exception representing situation when given Coupon code is invalid and
 * therefore cannot be returned to user.
 */
public final class CouponNotFoundForCodeException extends RuntimeException {

    private static final String MESSAGE_PATTERN = "Could not find coupon for code %s";

    public CouponNotFoundForCodeException(final String code) {
        super(String.format(MESSAGE_PATTERN, code));
    }
}
