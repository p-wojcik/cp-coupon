package pl.pwojcik.app.couponstudio.coupon.repository;

import static com.google.common.collect.ImmutableMap.of;
import static java.util.Collections.singletonList;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.UpdateOptions;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponScopesNotModifiedException;
import pl.pwojcik.app.couponstudio.coupon.exception.InvalidTenantException;

@Component
public class CouponActivityStatusRepository
{
	private static final Logger LOG = LoggerFactory.getLogger(CouponActivityStatusRepository.class);

	private static final String ID_FIELD = "_id";
	private static final String TENANT_FIELD = "tenant";
	private static final String ACTIVE_FIELD = "active";
	private static final String COUPON_SCOPE_QUERY = "scopes.$[element].active";
	private static final String LOCATION_ID_FIELD = "element.locationId";

	private static final String MONGO_SET_OPERATION = "$set";
	private static final String MONGO_EQUAL_OPERATION = "$eq";

	@Autowired
	private MongoConnector connector;

	/**
	 * Method for changing global 'active' flag of single Coupon
	 *
	 * @param tenant tenant
	 * @param id ID of coupon
	 * @param status new activity status
	 * @return true if operation succeeded, Single.error otherwise
	 */
	public Single<Boolean> changeActivityStatusForCoupon(final String tenant, final UUID id, final boolean status)
	{
		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id.toString(),//
				TENANT_FIELD, tenant//
		));
		final BasicDBObject update = new BasicDBObject(MONGO_SET_OPERATION,//
				new BasicDBObject(ACTIVE_FIELD, status)//
		);
		final long modifiedCount = connector.getCollection()//
				.updateOne(query, update)//
				.getModifiedCount();
		if (modifiedCount == 1)
		{
			return Single.just(true);
		}
		LOG.warn("Expected to modify 1 entry instead of {} entries while changing activity status to '{}'" +//
				"for coupon with id {} for tenant {}. ", modifiedCount, status, id.toString(), tenant);
		return Single.error(new CouponScopesNotModifiedException(tenant, id));
	}

	/**
	 * Method for changing global 'active' flag of all tenant's coupons
	 *
	 * @param tenant tenant
	 * @param status new activity status
	 * @return true if operation succeeded, Single.error otherwise
	 */
	public Single<Boolean> changeActivityStatusForAllCoupons(final String tenant, final boolean status)
	{
		final BasicDBObject query = new BasicDBObject(TENANT_FIELD, tenant);
		final BasicDBObject update = new BasicDBObject(MONGO_SET_OPERATION,//
				new BasicDBObject(ACTIVE_FIELD, status)//
		);
		connector.getCollection().updateMany(query, update);
		return Single.just(true);
	}

	/**
	 * Method for changing activity status of single Coupon only for specified location.
	 *
	 * @param tenant tenant
	 * @param id ID of Coupon
	 * @param locationId ID of locations where status has to be changed
	 * @param status new status
	 * @return true if operation succeeded, Single.error otherwise
	 */
	public Single<Boolean> changeActivityStatusForCouponInLocations(final String tenant, final String id,//
			final String locationId, final boolean status)
	{
		final BasicDBObject query = new BasicDBObject(of(//
				ID_FIELD, id, //
				TENANT_FIELD, tenant//
		));
		return changeActivityStatusInLocation(query, locationId, status)//
				.flatMap(result ->
				{
					if (result)
					{
						return Single.just(true);
					}
					LOG.warn("Collection {} not existing. Changing activity status for location in coupon with ID {} aborted" +//
							".", tenant, id);
					return Single.error(new InvalidTenantException("changeActivityStatusInCouponForLocation", tenant));
				});
	}

	/**
	 * Method for changing activity status of all tenant Coupons only for specified locations.
	 *
	 * @param tenant tenant
	 * @param locationId ID of locations where status has to be changed
	 * @param status new status
	 * @return true if operation succeeded, Single.error otherwise
	 */
	public Single<Boolean> changeActivityStatusForTenantCouponsInLocation(final String tenant, //
			final String locationId, final boolean status)
	{
		final BasicDBObject query = new BasicDBObject(TENANT_FIELD, tenant);
		return changeActivityStatusInLocation(query, locationId, status)//
				.flatMap(result ->
				{//
					if (result)
					{
						return Single.just(true);
					}
					LOG.warn("Collection {} not existing. Changing activity status for locations in tenant coupons aborted.",//
							tenant);
					return Single.error(new InvalidTenantException("changeActivityStatusInTenantCouponsForLocations", tenant));
				});
	}

	private Single<Boolean> changeActivityStatusInLocation(final BasicDBObject objectQuery,//
			final String locationId, final boolean status)
	{
		final BasicDBObject update = new BasicDBObject(MONGO_SET_OPERATION,//
				new BasicDBObject(COUPON_SCOPE_QUERY, status)//
		);

		final BasicDBObject arrayFilter = new BasicDBObject(LOCATION_ID_FIELD, //
				new BasicDBObject(MONGO_EQUAL_OPERATION, locationId)//
		);
		final UpdateOptions updateOptions = new UpdateOptions().arrayFilters(singletonList(arrayFilter));

		connector.getCollection().updateMany(objectQuery, update, updateOptions);
		return Single.just(true);
	}
}
