package pl.pwojcik.app.couponstudio.coupon.model.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.UUID;

public final class UUIDDeserializer extends JsonDeserializer<UUID> {

    @Override
    public UUID deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException {
        final String text = parser.getValueAsString();
        return UUID.fromString(text);
    }
}
