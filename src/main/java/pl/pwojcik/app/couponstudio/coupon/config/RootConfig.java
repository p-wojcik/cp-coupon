package pl.pwojcik.app.couponstudio.coupon.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.pwojcik.app.couponstudio.coupon")
public class RootConfig {
}