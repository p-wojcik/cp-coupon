package pl.pwojcik.app.couponstudio.coupon.exception;

/**
 * Exception representing situation when Coupon with given ID does not exist and
 * therefore cannot be returned to user.
 */
public final class CouponNotFoundException extends RuntimeException {

    private static final String MESSAGE_PATTERN = "Coupon with ID %s could not be found";
    private final String tenant;

    public CouponNotFoundException(final String id, final String tenant) {
        super(String.format(MESSAGE_PATTERN, id));
        this.tenant = tenant;
    }

    public String getTenant() {
        return tenant;
    }
}
