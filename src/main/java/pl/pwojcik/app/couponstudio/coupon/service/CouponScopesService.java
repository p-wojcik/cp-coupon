package pl.pwojcik.app.couponstudio.coupon.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponScopesRepository;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

@Component
public class CouponScopesService
{
	@Autowired
	private CouponValidator validator;

	@Autowired
	private CouponScopesRepository repository;

	public Single<Boolean> addScopesForCoupon(final Collection<CouponScope> scopes,//
			final String tenant, final String uuid)
	{

		return validator.validateCouponId(uuid)//
				.flatMap(id -> repository.addScopesToCoupon(scopes, tenant, id));
	}

	public Single<Boolean> removeScopesForCoupon(final Collection<String> locationIds,//
			final String tenant, final String uuid)
	{
		return validator.validateScopeIds(locationIds)//
				.flatMap(avoid -> validator.validateCouponId(uuid))//
				.flatMap(id -> repository.removeScopesInCoupon(locationIds, tenant, id));
	}
}
