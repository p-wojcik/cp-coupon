package pl.pwojcik.app.couponstudio.coupon.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;
import java.util.UUID;

/**
 * Class representing availability scope of single Coupon.
 */
public class CouponScope {

    private final UUID locationId;
    private final boolean active;

    @JsonCreator
    public CouponScope(@JsonProperty("locationId") final UUID locationId,
                       @JsonProperty("active") final boolean active) {
        this.locationId = locationId;
        this.active = active;
    }

    public UUID getLocationId() {
        return locationId;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("locationId", locationId)//
                .append("active", active)//
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(locationId)//
                .append(active)//
                .build();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (Objects.isNull(obj) || !obj.getClass().equals(getClass())) {
            return false;
        }
        final CouponScope that = CouponScope.class.cast(obj);
        return new EqualsBuilder()//
                .append(that.locationId, locationId)//
                .append(that.active, active)//
                .build();
    }
}
