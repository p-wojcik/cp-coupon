package pl.pwojcik.app.couponstudio.coupon.repository;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;

@Component
public class CouponCollectionScopesRepository
{
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String SCOPES_FIELD = "scopes";
	private static final String TENANT_FIELD = "tenant";
	private static final String LOCATION_ID_FIELD = "locationId";

	private static final String MONGO_IN_OPERATION = "$in";
	private static final String MONGO_PULL_OPERATION = "$pull";
	private static final String MONGO_EACH_OPERATION = "$each";
	private static final String MONGO_ADD_TO_SET_OPERATION = "$addToSet";

	@Autowired
	private MongoConnector connector;

	/**
	 * Method for adding scopes to all tenant coupons.
	 *
	 * @param scopes scopes to add
	 * @param tenant tenant
	 * @return true if scopes have been added, Single.error otherwise
	 */
	public Single<Boolean> addScopesToTenantCoupons(final Collection<CouponScope> scopes,//
			final String tenant)
	{
		final Object[] scopesArray = scopes.stream()//
				.map(scope -> OBJECT_MAPPER.convertValue(scope, Map.class))//
				.toArray();
		final BasicDBObject update = new BasicDBObject(MONGO_ADD_TO_SET_OPERATION,//
				new BasicDBObject(SCOPES_FIELD,//
						new BasicDBObject(MONGO_EACH_OPERATION, scopesArray)//
				)//
		);
		final BasicDBObject query = new BasicDBObject(TENANT_FIELD, tenant);
		connector.getCollection().updateMany(query, update);
		return Single.just(true);
	}

	/**
	 * Method for removing scopes from all tenant coupons
	 *
	 * @param locationIds scopes to remove
	 * @param tenant tenant
	 * @return true if scopes have been removed, Single.error otherwise
	 */
	public Single<Boolean> removeScopesInTenantCoupons(final Collection<String> locationIds,//
			final String tenant)
	{

		final String[] locationIdsArray = locationIds.toArray(new String[]{});
		final BasicDBObject update = new BasicDBObject(MONGO_PULL_OPERATION,//
				new BasicDBObject(SCOPES_FIELD,//
						new BasicDBObject(LOCATION_ID_FIELD,//
								new BasicDBObject(MONGO_IN_OPERATION, locationIdsArray)//
						)//
				)//
		);
		final BasicDBObject query = new BasicDBObject(TENANT_FIELD, tenant);
		connector.getCollection().updateMany(query, update);
		return Single.just(true);
	}
}
