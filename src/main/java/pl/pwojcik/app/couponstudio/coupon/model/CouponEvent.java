package pl.pwojcik.app.couponstudio.coupon.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import pl.pwojcik.app.couponstudio.coupon.model.serialization.UUIDDeserializer;
import pl.pwojcik.app.couponstudio.coupon.model.serialization.UUIDSerializer;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Class representing single Coupon.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponEvent {

    @JsonSerialize(using = UUIDSerializer.class)
    @JsonDeserialize(using = UUIDDeserializer.class)
    private final UUID id;
    private final String code;
    private final String name;
    private final String description;
    private final boolean mediaAvailable;
    private final boolean active;
    private final Set<CouponScope> scopes;

    @JsonCreator
    public CouponEvent(@JsonProperty("_id") final UUID id,//
                       @JsonProperty("code") final String code,//
                       @JsonProperty("name") final String name,//
                       @JsonProperty("description") final String description,//
                       @JsonProperty("mediaAvailable") final boolean mediaAvailable,//
                       @JsonProperty("active") final boolean active,//
                       @JsonProperty("scopes") final Set<CouponScope> scopes) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.mediaAvailable = mediaAvailable;
        this.active = active;
        this.scopes = scopes;
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<CouponScope> getScopes() {
        return scopes;
    }

    public boolean isMediaAvailable() {
        return mediaAvailable;
    }

    public boolean isActive()
    {
        return active;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)//
                .append("code", code)//
                .append("name", name)//
                .append("description", description)//
                .append("scopes", scopes)//
                .append("mediaAvailable", mediaAvailable)//
                .append("active", active)//
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)//
                .append(code)//
                .append(name)//
                .append(description)//
                .append(scopes)//
                .append(mediaAvailable)//
                .append(active)//
                .build();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (Objects.isNull(obj) || !obj.getClass().equals(getClass())) {
            return false;
        }
        final CouponEvent that = CouponEvent.class.cast(obj);
        return new EqualsBuilder()//
                .append(that.id, id)//
                .append(that.code, code)//
                .append(that.name, name)//
                .append(that.description, description)//
                .append(that.scopes, scopes)//
                .append(that.mediaAvailable, mediaAvailable)//
                .append(that.active, active)//
                .build();
    }
}
