package pl.pwojcik.app.couponstudio.coupon.model.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.UUID;

public final class UUIDSerializer extends JsonSerializer<UUID> {

    @Override
    public void serialize(final UUID value, final JsonGenerator gen,
                          final SerializerProvider serializers) throws IOException {
        gen.writeString(value.toString());
    }

}
