package pl.pwojcik.app.couponstudio.coupon.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.tuple.Pair;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundForCodeException;

import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedList;

/**
 *
 */
public class EncodingUtility {

    /**
     *
     * @param tenant
     * @param components
     * @return
     */
    public static String encodeForTenant(final String tenant, final String... components) {
        final LinkedList<String> elementsToJoin = new LinkedList<>(Arrays.asList(components));
        elementsToJoin.addFirst(tenant);

        final String plainCode = String.join(".", elementsToJoin);
        final String code = DigestUtils.sha256Hex(plainCode);
        final String tenantWithCode = String.join("|", tenant, code);
        return Base64.getEncoder().encodeToString(tenantWithCode.getBytes());
    }

    /**
     *
     * @param code
     * @return
     */
    static Pair<String, String> decodeTenantAndAccessCode(final String code) {
        final byte[] encodedBytes = Base64.getDecoder().decode(code);
        final String tenantWithCode = new String(encodedBytes);
        final String[] split = tenantWithCode.split("\\|");
        if (split.length != 2) {
            throw new CouponNotFoundForCodeException(code);
        }
        return Pair.of(split[0], split[1]);
    }
}
