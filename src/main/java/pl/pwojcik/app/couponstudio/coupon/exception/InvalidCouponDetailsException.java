package pl.pwojcik.app.couponstudio.coupon.exception;

/**
 * Exception class indicating that some details of Coupon are not valid.
 */
public final class InvalidCouponDetailsException extends RuntimeException {

    public InvalidCouponDetailsException(final String message) {
        super(message);
    }
}
