package pl.pwojcik.app.couponstudio.coupon.service;

import static java.util.Collections.singletonList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponActivityStatusRepository;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

@Component
public class CouponActivityStatusService
{
	private static final Logger LOG = LoggerFactory.getLogger(CouponActivityStatusService.class);

	@Autowired
	private CouponValidator validator;

	@Autowired
	private CouponActivityStatusRepository repository;

	public Single<Boolean> enableCoupon(final String tenant, final String uuid)
	{
		return validator.validateCouponId(uuid)//
				.doOnSuccess(id -> LOG.debug("Enabling coupon for tenant: {}, ID: {}.", tenant, id))//
				.flatMap(id -> repository.changeActivityStatusForCoupon(tenant, id, true));
	}

	public Single<Boolean> disableCoupon(final String tenant, final String uuid)
	{
		return validator.validateCouponId(uuid)//
				.doOnSuccess(id -> LOG.debug("Disabling coupon for tenant: {}, ID: {}.", tenant, id))//
				.flatMap(id -> repository.changeActivityStatusForCoupon(tenant, id, false));
	}

	public Single<Boolean> enableAllCoupons(final String tenant)
	{
		return repository.changeActivityStatusForAllCoupons(tenant, true);
	}

	public Single<Boolean> disableAllCoupons(final String tenant)
	{
		return repository.changeActivityStatusForAllCoupons(tenant, false);
	}

	public Single<Boolean> enableCouponInSpecifiedLocation(final String tenant, final String id, //
			final String locationId)
	{
		return changeCouponActivityStatusInSpecifiedLocation(tenant, id, locationId, true);
	}

	public Single<Boolean> disableCouponInSpecifiedLocation(final String tenant, final String id, //
			final String locationId)
	{
		return changeCouponActivityStatusInSpecifiedLocation(tenant, id, locationId, false);
	}

	private Single<Boolean> changeCouponActivityStatusInSpecifiedLocation(final String tenant, final String id, //
			final String locationId, final boolean status)
	{
		return validator.validateCouponId(id)//
				.flatMap(avoid -> validator.validateScopeIds(singletonList(locationId)))//
				.flatMap(avoid -> repository.changeActivityStatusForCouponInLocations(tenant, id, locationId, status));
	}

	public Single<Boolean> enableTenantCouponsInSpecifiedLocation(final String tenant, final String locationId)
	{
		return changeTenantCouponsActivityStatusInSpecifiedLocation(tenant, locationId, true);
	}

	public Single<Boolean> disableTenantCouponsInSpecifiedLocation(final String tenant, final String locationId)
	{
		return changeTenantCouponsActivityStatusInSpecifiedLocation(tenant, locationId, false);
	}

	private Single<Boolean> changeTenantCouponsActivityStatusInSpecifiedLocation(final String tenant, //
			final String locationId, final boolean status)
	{
		return validator.validateScopeIds(singletonList(locationId))//
				.flatMap(avoid -> repository.changeActivityStatusForTenantCouponsInLocation(tenant, locationId, status));
	}
}
