package pl.pwojcik.app.couponstudio.coupon.repository;

import static com.google.common.collect.ImmutableMap.of;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * Bridge between application and MongoDB database.
 */
@Component
final class MongoConnector
{
	private static final Logger LOG = LoggerFactory.getLogger(MongoConnector.class);
	private static final Integer DEFAULT_CONNECTION_TIMEOUT_MS = 2000;
	private static final int ASCENDING_ORDER = 1;
	private static final String ID_FIELD = "id";
	private static final String TENANT_FIELD = "tenant";
	private static final String CODE_FIELD = "code";

	private static final String COUPONS_COLLECTION = "coupons";

	private MongoClient client;
	private MongoCollection<Document> collection;

	@Value("${mongo.uri}")
	private String uri;

	@Value("${mongo.source}")
	private String source;

	@PostConstruct
	void setup()
	{
		final MongoClientOptions.Builder optionsBuilder = MongoClientOptions.builder()//
				.serverSelectionTimeout(DEFAULT_CONNECTION_TIMEOUT_MS)//
				.heartbeatConnectTimeout(DEFAULT_CONNECTION_TIMEOUT_MS)//
				.heartbeatSocketTimeout(DEFAULT_CONNECTION_TIMEOUT_MS)//
				.connectTimeout(DEFAULT_CONNECTION_TIMEOUT_MS)//
				.socketTimeout(DEFAULT_CONNECTION_TIMEOUT_MS);

		client = new MongoClient(new MongoClientURI(uri, optionsBuilder));
		final MongoDatabase mongo = client.getDatabase(source);
		collection = mongo.getCollection(COUPONS_COLLECTION);
		createIndices();

		LOG.info("Successfully connected to MongoDB, database {}, collection {}", source, COUPONS_COLLECTION);
	}

	@PreDestroy
	void destroy()
	{
		client.close();
	}

	private void createIndices()
	{
		final BasicDBObject idIndex = new BasicDBObject(of(//
				ID_FIELD, ASCENDING_ORDER,//
				TENANT_FIELD, ASCENDING_ORDER//
		));
		final BasicDBObject codeIndex = new BasicDBObject(of(//
				CODE_FIELD, ASCENDING_ORDER,//
				TENANT_FIELD, ASCENDING_ORDER//
		));
		collection.createIndex(idIndex);
		collection.createIndex(codeIndex);
		LOG.info("Created indexes in database {} for collection {}", source,//
				collection.getNamespace().getCollectionName());
	}

	MongoCollection<Document> getCollection()
	{
		return collection;
	}
}
