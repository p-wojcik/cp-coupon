package pl.pwojcik.app.couponstudio.coupon.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import io.reactivex.Observable;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;

@Component
public class CouponCollectionRepository
{
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String TENANT_FIELD = "tenant";

	@Autowired
	private MongoConnector connector;

	/**
	 * Method for fetching all tenant's coupons.
	 *
	 * @param tenant tenant
	 * @return Observable of Coupon, Observable.error in case of failure
	 */
	public Observable<CouponEvent> getAllCoupons(final String tenant)
	{
		final BasicDBObject query = new BasicDBObject(TENANT_FIELD, tenant);
		final FindIterable<Document> result = connector.getCollection().find(query);
		return Observable.fromIterable(result)//
				.map(item -> OBJECT_MAPPER.convertValue(item, CouponEvent.class));
	}
}
