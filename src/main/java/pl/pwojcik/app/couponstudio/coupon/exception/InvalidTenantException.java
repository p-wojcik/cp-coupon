package pl.pwojcik.app.couponstudio.coupon.exception;

/**
 * Exception thrown when operation on database cannot be processed due to missing collection for given tenant.
 */
public final class InvalidTenantException extends RuntimeException {

    private static final String PATTERN = "Could not process operation %s on tenant %s due to missing collection.";

    public InvalidTenantException(final String operation, final String tenant) {
        super(String.format(PATTERN, operation, tenant));
    }
}
