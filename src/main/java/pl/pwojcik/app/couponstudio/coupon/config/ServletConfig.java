package pl.pwojcik.app.couponstudio.coupon.config;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.util.stream.Stream;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan("pl.pwojcik.app.couponstudio.coupon")
@PropertySource("classpath:default.properties")
public class ServletConfig extends WebMvcConfigurerAdapter
{
	private static final String PATTERN_ALL_PATHS = "/**";

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
	{
		configurer.enable();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry)
	{
		super.addResourceHandlers(registry);
	}

	@Override
	public void addCorsMappings(final CorsRegistry registry)
	{
		final String[] allowedMethods = Stream.of(GET, POST, PUT, DELETE, PATCH, OPTIONS)//
				.map(Enum::name)//
				.toArray(String[]::new);

		registry.addMapping(PATTERN_ALL_PATHS)//
				.allowedMethods(allowedMethods);
	}
}