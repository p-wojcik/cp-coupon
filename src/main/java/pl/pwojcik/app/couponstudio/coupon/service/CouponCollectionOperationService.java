package pl.pwojcik.app.couponstudio.coupon.service;

import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponCollectionRepository;

import java.util.List;

@Component
public class CouponCollectionOperationService
{
	@Autowired
	private CouponCollectionRepository repository;

	public Single<List<CouponEvent>> getAllCoupons(final String tenant) {
		return repository.getAllCoupons(tenant)//
				.toList();
	}

}
