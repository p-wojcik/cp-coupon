/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.repository;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pl.pwojcik.app.couponstudio.coupon.config.RootConfig;
import pl.pwojcik.app.couponstudio.coupon.config.ServletConfig;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundForCodeException;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.EncodingUtility;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes =  {RootConfig.class, ServletConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource("classpath:default-test.properties")
public class CouponRepositoryTest
{
	private static final String TENANT = "icarus";

	@Autowired
	private CouponRepository couponRepository;

	@ClassRule
	public static EmbeddedMongoClassRule mongoClassRule = new EmbeddedMongoClassRule();

	@After
	public void clearMongo()
	{
		couponRepository.deleteAllDataFromBackingCollection();
	}

	@Test
	public void shouldCreateAndFindCouponById() {
		// Given
		final UUID id = UUID.randomUUID();
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		event.setId(id);
		event.setCode(EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), id.toString()));

		// When
		final Boolean result = couponRepository.create(TENANT, event).blockingGet();

		// Then
		assertThat(result).isTrue();
		final CouponEvent getResult = couponRepository.getById(TENANT, id).blockingGet();

		assertThat(getResult.getCode()).isEqualTo(event.getCode());
		assertThat(getResult.getDescription()).isEqualTo(event.getDescription());
		assertThat(getResult.getId()).isEqualTo(event.getId());
		assertThat(getResult.getName()).isEqualTo(event.getName());
		assertThat(getResult.getScopes()).isEqualTo(scopes);
	}

	@Test
	public void shouldCreateAndFindCouponByCode() {
		// Given
		final UUID id = UUID.randomUUID();
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		final String code = EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), id.toString());

		event.setId(id);
		event.setCode(code);

		// When
		final Boolean result = couponRepository.create(TENANT, event).blockingGet();

		// Then
		assertThat(result).isTrue();
		final CouponEvent getResult = couponRepository.getByCode(TENANT, code).blockingGet();

		assertThat(getResult.getCode()).isEqualTo(event.getCode());
		assertThat(getResult.getDescription()).isEqualTo(event.getDescription());
		assertThat(getResult.getId()).isEqualTo(event.getId());
		assertThat(getResult.getName()).isEqualTo(event.getName());
		assertThat(getResult.getScopes()).isEqualTo(scopes);
	}

	@Test(expected = CouponNotFoundException.class)
	public void shouldDeleteCoupon() {
		// Given
		final UUID id = UUID.randomUUID();
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		event.setId(id);
		event.setCode(EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), id.toString()));
		final Boolean createResult = couponRepository.create(TENANT, event).blockingGet();
		assertThat(createResult).isTrue();
		couponRepository.getById(TENANT, id).blockingGet();

		// When
		final Boolean deleteResult = couponRepository.delete(TENANT, id.toString()).blockingGet();

		// Then
		assertThat(deleteResult).isTrue();
		couponRepository.getById(TENANT, id).blockingGet();
	}

	@Test(expected = CouponNotFoundException.class)
	public void shouldThrowExceptionWhenCouponNotFoundByID() {
		// Given
		final UUID id = UUID.randomUUID();

		// When
		couponRepository.getById(TENANT, id).blockingGet();
	}

	@Test(expected = CouponNotFoundForCodeException.class)
	public void shouldThrowExceptionWhenCouponNotFoundByCode() {
		// Given
		final String code = UUID.randomUUID().toString();

		// When
		couponRepository.getByCode(TENANT, code).blockingGet();
	}

}
