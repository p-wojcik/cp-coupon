/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.repository;

import org.junit.rules.ExternalResource;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

/**
 * Common methods for managing embedded mongo used in tests.
 * This rule is evaluated before and after evaluation of test class.
 */
final class EmbeddedMongoClassRule extends ExternalResource
{
	private static final String BINDING_IP = "localhost";
	private static final int BINDING_PORT = 8090;
	private static volatile MongodExecutable mongodExecutable;

	@Override
	protected void before() throws Throwable {
		final MongodStarter starter = MongodStarter.getDefaultInstance();
		final IMongodConfig mongodConfig = new MongodConfigBuilder()//
						.version(Version.Main.PRODUCTION)//
						.net(new Net(BINDING_IP, BINDING_PORT, Network.localhostIsIPv6()))//
						.build();

		mongodExecutable = starter.prepare(mongodConfig);
		mongodExecutable.start();
	}

	@Override
	protected void after() {
		mongodExecutable.stop();
	}
}