/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.repository;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pl.pwojcik.app.couponstudio.coupon.config.RootConfig;
import pl.pwojcik.app.couponstudio.coupon.config.ServletConfig;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.EncodingUtility;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class, ServletConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource("classpath:default-test.properties")
public class CouponCollectionScopesRepositoryTest
{
	private static final String TENANT = "icarus";
	private static final UUID LOCATION_ID = UUID.randomUUID();

	@Autowired
	private CouponRepository couponRepository;

	@Autowired
	private CouponCollectionRepository couponCollectionRepository;

	@Autowired
	private CouponCollectionScopesRepository couponCollectionScopesRepository;

	@ClassRule
	public static EmbeddedMongoClassRule mongoClassRule = new EmbeddedMongoClassRule();

	@After
	public void clearMongo()
	{
		couponRepository.deleteAllDataFromBackingCollection();
	}

	@Test
	public void shouldAddScopesToAllCoupons()
	{
		// Given
		storeMultipleEvents();
		final UUID newScopeId = UUID.randomUUID();
		final CouponScope newScope = new CouponScope(newScopeId, true);
		final List<CouponScope> scopesToAdd = Collections.singletonList(newScope);

		// When
		final Boolean result = couponCollectionScopesRepository.addScopesToTenantCoupons(scopesToAdd, TENANT).blockingGet();

		// Then
		assertThat(result).isTrue();
		final List<CouponEvent> couponEvents = couponCollectionRepository.getAllCoupons(TENANT).toList().blockingGet();
		assertThat(couponEvents).isNotEmpty();

		for (final CouponEvent event : couponEvents) {
			final Set<CouponScope> scopes = event.getScopes();
			assertThat(scopes).contains(newScope);
		}
	}

	@Test
	public void shouldRemoveScopesFromAllCoupons()
	{
		// Given
		storeMultipleEvents();
		final UUID newScopeId = UUID.randomUUID();
		final CouponScope newScope = new CouponScope(newScopeId, true);
		final List<CouponScope> scopesToAdd = Collections.singletonList(newScope);

		// When
		final Boolean result = couponCollectionScopesRepository.addScopesToTenantCoupons(scopesToAdd, TENANT).blockingGet();

		// Then
		assertThat(result).isTrue();
		final List<CouponEvent> couponEvents = couponCollectionRepository.getAllCoupons(TENANT).toList().blockingGet();
		assertThat(couponEvents).isNotEmpty();

		for (final CouponEvent event : couponEvents) {
			final Set<CouponScope> scopes = event.getScopes();
			assertThat(scopes).contains(newScope);
		}
	}

	@Test
	public void shouldRemoveScopeFromAllCoupons()
	{
		// Given
		storeMultipleEvents();
		final List<String> scopesToRemove = Collections.singletonList(LOCATION_ID.toString());

		// When
		final Boolean result = couponCollectionScopesRepository.removeScopesInTenantCoupons(scopesToRemove, TENANT).blockingGet();

		// Then
		assertThat(result).isTrue();
		final List<CouponEvent> couponEvents = couponCollectionRepository.getAllCoupons(TENANT).toList().blockingGet();
		assertThat(couponEvents).isNotEmpty();

		for (final CouponEvent event : couponEvents) {
			final Set<CouponScope> scopes = event.getScopes();
			assertThat(scopes).isEmpty();
		}
	}

	private void storeMultipleEvents() {
		final List<CreateCouponEvent> events = Arrays.asList(prepareEvent(), prepareEvent(), prepareEvent());
		final boolean allSaved = events.stream()//
				.map(event -> couponRepository.create(TENANT, event).blockingGet())//
				.allMatch(result -> result);
		assertThat(allSaved).isTrue();
	}

	private CreateCouponEvent prepareEvent()
	{
		final UUID id = UUID.randomUUID();
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(LOCATION_ID, true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		event.setId(id);
		event.setCode(EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), id.toString()));

		return event;
	}
}
