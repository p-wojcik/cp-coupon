/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.service;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundException;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponNotFoundForCodeException;
import pl.pwojcik.app.couponstudio.coupon.exception.InvalidCouponDetailsException;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.repository.CouponRepository;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

@RunWith(MockitoJUnitRunner.class)
public class CouponBasicOperationsServiceTest
{
	private static final String TEST_TENANT = "kamaz";
	private static final String URI = "http://localhost";

	@Spy
	private CouponValidator validator = new CouponValidator();

	@Mock
	private CouponRepository repository;

	@InjectMocks
	private CouponBasicOperationsService service = new CouponBasicOperationsService();

	@Test
	public void shouldCreateCoupon()
	{
		// Given
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);

		when(repository.create(eq(TEST_TENANT), eq(event))).thenReturn(Single.just(true));

		// When
		final String locationHeader = service.create(TEST_TENANT, event, URI).blockingGet();

		// Then
		assertThat(locationHeader).contains("http://localhost/");
		final String potentialUUID = locationHeader.substring(locationHeader.lastIndexOf("/") + 1);
		try
		{
			UUID.fromString(potentialUUID);
		}
		catch (final IllegalArgumentException exception)
		{
			fail("UUID is invalid.", exception);
		}
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenCodeSecretIsEmpty()
	{
		// Given
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent("", "couponName", "coupon description",//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenCodeSecretIsNull()
	{
		// Given
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent("", "couponName", "coupon description",//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenNameIsEmpty()
	{
		// Given
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "", "coupon description",//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenNameIsNull()
	{
		// Given
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, null, "coupon description",//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenDescriptionIsEmpty()
	{
		// Given
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "",//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldNotCreateCouponWhenDescriptionIsNull()
	{
		// Given
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", null,//
				scopes, false, true);

		// When
		service.create(TEST_TENANT, event, URI).blockingGet();
	}

	@Test
	public void shouldGetCouponById()
	{
		// Given
		final UUID id = UUID.fromString("87a8354b-ed47-4e4b-94c7-4ca5393e9c0c");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CouponEvent expectedEvent = new CouponEvent(id, "abc", "couponName", "description",//
				false, true, scopes);

		when(repository.getById(eq(TEST_TENANT), eq(id))).thenReturn(Single.just(expectedEvent));

		// When
		final CouponEvent result = service.getById(TEST_TENANT, id.toString()).blockingGet();

		// Then
		assertThat(result).isEqualTo(expectedEvent);
	}

	@Test
	public void shouldGetCouponByCode()
	{
		// Given
		final UUID uuid = UUID.randomUUID();
		final String code = EncodingUtility.encodeForTenant(TEST_TENANT, "secret", uuid.toString());
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CouponEvent expectedEvent = new CouponEvent(uuid, code, "couponName", "description",//
				false, true, scopes);

		when(repository.getByCode(eq(TEST_TENANT), eq(code))).thenReturn(Single.just(expectedEvent));

		// When
		final CouponEvent result = service.getByCode(code).blockingGet();

		// Then
		assertThat(result).isEqualTo(expectedEvent);
	}

	@Test(expected = CouponNotFoundException.class)
	public void shouldThrowExceptionWhenCouponNotFoundByID()
	{
		// Given
		final UUID id = UUID.randomUUID();
		when(repository.getById(eq(TEST_TENANT), eq(id)))//
				.thenReturn(Single.error(new CouponNotFoundException(id.toString(), TEST_TENANT)));

		// When
		service.getById(TEST_TENANT, id.toString()).blockingGet();
	}

	@Test(expected = CouponNotFoundForCodeException.class)
	public void shouldThrowExceptionWhenCouponNotFoundByCode()
	{
		// Given
		final UUID uuid = UUID.randomUUID();
		final String code = EncodingUtility.encodeForTenant(TEST_TENANT, "secret", uuid.toString());
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CouponEvent expectedEvent = new CouponEvent(uuid, code, "couponName", "description",//
				false, true, scopes);

		when(repository.getByCode(eq(TEST_TENANT), eq(code)))//
				.thenReturn(Single.error(new CouponNotFoundForCodeException(code)));

		// When
		final CouponEvent result = service.getByCode(code).blockingGet();

		// Then
		assertThat(result).isEqualTo(expectedEvent);
	}

	@Test(expected = CouponNotFoundForCodeException.class)
	public void shouldThrowExceptionWhenCannotDecipherGivenCode()
	{
		// Given
		final String code = "tooSimpleToDecipher";

		// When
		service.getByCode(code).blockingGet();
	}

	@Test
	public void shouldDeleteCoupon() {
		// Given
		final String uuid = UUID.randomUUID().toString();
		when(repository.delete(TEST_TENANT, uuid)).thenReturn(Single.just(true));

		// When
		final Boolean result = service.delete(TEST_TENANT, uuid).blockingGet();

		// Then
		assertThat(result).isTrue();
	}
}
