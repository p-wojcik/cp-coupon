/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.repository;

import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.pwojcik.app.couponstudio.coupon.config.RootConfig;
import pl.pwojcik.app.couponstudio.coupon.config.ServletConfig;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.EncodingUtility;

import java.util.*;
import java.util.stream.Collectors;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class, ServletConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource("classpath:default-test.properties")
public class CouponCollectionRepositoryTest
{
	private static final String TENANT = "icarus";

	@Autowired
	private CouponRepository couponRepository;

	@Autowired
	private CouponCollectionRepository couponCollectionRepository;

	@ClassRule
	public static EmbeddedMongoClassRule mongoClassRule = new EmbeddedMongoClassRule();

	@After
	public void clearMongo()
	{
		couponRepository.deleteAllDataFromBackingCollection();
	}

	@Test
	public void shouldCreateMultipleCouponsAndGetAll()
	{
		// Given
		final List<CreateCouponEvent> events = Arrays.asList(prepareEvent(), prepareEvent(), prepareEvent());
		//
		final boolean allSaved = events.stream()//
				.allMatch(event -> couponRepository.create(TENANT, event).blockingGet());
		assertThat(allSaved).isTrue();

		final List<String> idsList = events.stream()//
				.map(CreateCouponEvent::getId)//
				.map(UUID::toString)//
				.collect(Collectors.toList());

		// When
		final List<String> getResult = couponCollectionRepository.getAllCoupons(TENANT)//
				.map(CouponEvent::getId)
				.map(UUID::toString)
				.toList()//
				.blockingGet();

		// Then
		assertThat(getResult).hasSize(idsList.size());
		assertThat(getResult).isEqualTo(idsList);
	}

	private CreateCouponEvent prepareEvent()
	{
		final UUID id = UUID.randomUUID();
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(UUID.randomUUID(), true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		event.setId(id);
		event.setCode(EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), id.toString()));

		return event;
	}
}

