/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.repository;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pl.pwojcik.app.couponstudio.coupon.config.RootConfig;
import pl.pwojcik.app.couponstudio.coupon.config.ServletConfig;
import pl.pwojcik.app.couponstudio.coupon.exception.CouponScopesNotModifiedException;
import pl.pwojcik.app.couponstudio.coupon.model.CouponEvent;
import pl.pwojcik.app.couponstudio.coupon.model.CouponScope;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.EncodingUtility;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class, ServletConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource("classpath:default-test.properties")
public class CouponScopesRepositoryTest
{
	private static final String TENANT = "icarus";
	private static final UUID COUPON_ID = UUID.randomUUID();
	private static final UUID LOCATION_ID = UUID.randomUUID();

	@Autowired
	private CouponScopesRepository couponScopesRepository;

	@Autowired
	private CouponRepository couponRepository;

	@ClassRule
	public static EmbeddedMongoClassRule mongoClassRule = new EmbeddedMongoClassRule();

	@After
	public void clearMongo()
	{
		couponRepository.deleteAllDataFromBackingCollection();
	}

	@Test
	public void shouldAddScopeToCoupon()
	{
		// Given
		storeCoupon();
		final UUID newScopeID = UUID.randomUUID();
		final CouponScope scopeToAdd = new CouponScope(newScopeID, true);

		// When
		final Boolean result = couponScopesRepository.addScopesToCoupon(singletonList(scopeToAdd), TENANT, COUPON_ID)//
				.blockingGet();

		// Then
		assertThat(result).isTrue();
		final CouponEvent getResult = couponRepository.getById(TENANT, COUPON_ID).blockingGet();
		assertThat(getResult.getId()).isEqualTo(COUPON_ID);

		final Set<UUID> locationIds = getResult.getScopes().stream()//
				.map(CouponScope::getLocationId)
				.collect(Collectors.toSet());

		assertThat(locationIds).contains(LOCATION_ID, newScopeID);
	}

	@Test(expected = CouponScopesNotModifiedException.class)
	public void shouldThrowExceptionWhenTryingToAddScopeToNotExistingCoupon()
	{
		// Given
		storeCoupon();
		final UUID newScopeID = UUID.randomUUID();
		final CouponScope scopeToAdd = new CouponScope(newScopeID, true);

		// When
		couponScopesRepository.addScopesToCoupon(singletonList(scopeToAdd), TENANT, UUID.randomUUID()).blockingGet();
	}

	@Test
	public void shouldRemoveScopeFromCoupon()
	{
		// Given
		storeCoupon();
		final List<String> scopesToRemove = singletonList(LOCATION_ID.toString());

		// When
		final Boolean result = couponScopesRepository.removeScopesInCoupon(scopesToRemove,//
				TENANT, COUPON_ID)//
				.blockingGet();

		// Then
		assertThat(result).isTrue();
		final CouponEvent getResult = couponRepository.getById(TENANT, COUPON_ID).blockingGet();
		assertThat(getResult.getId()).isEqualTo(COUPON_ID);

		final Set<CouponScope> scopes = getResult.getScopes();

		assertThat(scopes).isEmpty();
	}

	@Test(expected = CouponScopesNotModifiedException.class)
	public void shouldThrowExceptionWhenTryingToRemoveScopeFromNotExistingCoupon()
	{
		// Given
		storeCoupon();
		final String newScopeID = UUID.randomUUID().toString();

		// When
		couponScopesRepository.removeScopesInCoupon(singletonList(newScopeID), TENANT, UUID.randomUUID()).blockingGet();
	}

	private void storeCoupon()
	{
		final String codeSecret = UUID.randomUUID().toString().replaceAll("-", "");
		final Set<CouponScope> scopes = Collections.singleton(new CouponScope(LOCATION_ID, true));
		final CreateCouponEvent event = new CreateCouponEvent(codeSecret, "couponName", "coupon description",//
				scopes, false, true);
		event.setId(COUPON_ID);
		event.setCode(EncodingUtility.encodeForTenant(TENANT, event.getCodeSecret(), COUPON_ID.toString()));

		final Boolean result = couponRepository.create(TENANT, event).blockingGet();
		assertThat(result).isTrue();
	}
}
