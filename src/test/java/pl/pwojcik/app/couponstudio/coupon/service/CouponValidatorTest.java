/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package pl.pwojcik.app.couponstudio.coupon.service;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import org.junit.Test;

import pl.pwojcik.app.couponstudio.coupon.exception.InvalidCouponDetailsException;
import pl.pwojcik.app.couponstudio.coupon.model.CreateCouponEvent;
import pl.pwojcik.app.couponstudio.coupon.service.validation.CouponValidator;

public class CouponValidatorTest
{
	private CouponValidator validator = new CouponValidator();

	@Test
	public void shouldValidateCoupon()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("asd", "couponName",//
				"coupon description", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenNameIsEmpty()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("asd", "",//
				"coupon description", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenNameIsNull()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("asd", null,//
				"coupon description", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenDescriptionIsEmpty()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("asd", "couponName",//
				"", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenDescriptionIsNull()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("asd", "couponName",//
				null, Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenCodeSecretIsEmpty()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent("", "couponName",//
				"coupon description", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenCodeSecretIsNull()
	{
		// Given
		final CreateCouponEvent event = new CreateCouponEvent(null, "couponName",//
				"coupon description", Collections.emptySet(), false, true);

		// When
		final CreateCouponEvent result = validator.validateCoupon(event).blockingGet();

		// Then
		assertThat(result).isEqualTo(event);
	}

	@Test
	public void shouldValidateCouponId() {
		// Given
		final UUID uuid = UUID.randomUUID();

		// When
		final UUID result = validator.validateCouponId(uuid.toString()).blockingGet();

		// Then
		assertThat(result).isEqualTo(uuid);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenGivenUUIDIsInvalid() {
		// Given
		final String uuid = "kamaz";

		// When
		validator.validateCouponId(uuid).blockingGet();
	}

	@Test
	public void shouldValidateScopeIds() {

		// Given
		final String firstID = UUID.randomUUID().toString();
		final String secondID = UUID.randomUUID().toString();

		// When
		final Collection<String> result = validator.validateScopeIds(Arrays.asList(firstID, secondID)).blockingGet();

		// Then
		assertThat(result).contains(firstID, secondID);
	}

	@Test(expected = InvalidCouponDetailsException.class)
	public void shouldThrowExceptionWhenOneOfScopeIdsIsInvalid() {

		//Given
		final String firstID = UUID.randomUUID().toString();
		final String secondID = "kamaz";

		// When
		validator.validateScopeIds(Arrays.asList(firstID, secondID)).blockingGet();
	}

	@Test
	public void shouldValidateScopeIdsWithEmptyCollectionAsInput() {
		// When
		final Collection<String> result = validator.validateScopeIds(Collections.emptyList()).blockingGet();

		// Then
		assertThat(result).isEmpty();
	}
}
