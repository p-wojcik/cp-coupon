package pl.pwojcik.app.couponstudio.coupon

import groovyx.net.http.RESTClient
import org.junit.Test
import spock.lang.Shared
import spock.lang.Specification

import static groovyx.net.http.ContentType.JSON
import static java.util.Objects.nonNull
import static org.apache.http.HttpStatus.SC_OK

class CouponOperationsSpecification extends Specification {

	@Shared
	def restClient = new RESTClient("http://localhost:8080", JSON)

	String pathPrefix = '/cp-coupon'

	@Test
	def "Should create and get coupons by ID"() {
		given:
		def payload =
				[
						"codeSecret"    : "couponSecret",
						"name"          : "coupon_name",
						"description"   : "coupon_description",
						"scopes"        : [
								[
										"locationId": "fc73deca-0eef-4538-8759-b0bf230e88cc",
										"active"    : true
								],
								[
										"locationId": "fa67f14a-2396-41c3-af08-0cb0d3e6d55b",
										"active"    : true
								],
								[
										"locationId": "46fc799d-f254-453a-b40b-bb2fb98a49f1",
										"active"    : true
								]
						],
						"mediaAvailable": false,
						"active" : true
				]

		when:
		def headers = ['cp-tenant': 'test', 'Content-Type': 'application/json']
		def postResponse = restClient.post(path: pathPrefix + "/coupon", headers: headers, body: payload)

		then:
		postResponse.status == 201
		nonNull(postResponse.headers.Location)
		String locationHeader = postResponse.headers.Location;
		locationHeader.contains(pathPrefix)
		String id = locationHeader.substring(locationHeader.lastIndexOf("/")+1)

		def getResponse = restClient.get(path: pathPrefix + '/coupon/'+id, headers: headers)
		getResponse.status == SC_OK
		nonNull(getResponse.data)
		def savedObject = getResponse.data
		savedObject.name == 'coupon_name'
		savedObject.description == 'coupon_description'

		cleanup:
		restClient.delete(path: pathPrefix + '/coupon/all', headers: headers)
	}

}