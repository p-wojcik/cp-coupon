# Coupon Service

## Responsibilities
- add, get, delete coupons,
- get, delete all coupons for tenant,
- manage scope of single coupon (add, remove locations)
- manage scope of all tenant coupons (add, remove locations)
- enable & disable single coupon
- enable & disable all tenant coupons
- enable & disable single tenant only in specified location
- enable & disable all tenant coupons only in specified location

## How to run
`./gradlew appRun`

## Spock tests
To run spock tests there must be running instance of the application first.

## Endpoints
Prefix for all endpoints : `/cp-coupon` <br/>
Necessary header: `cp-tenant : test`

**1. Create coupon** <br/>
`POST /coupon`
```
{
	"codeSecret" : "a3borsuk",
	"name" : "a3borsuk",
	"description" : "3 borsuk",
	"scopes" : [
		{
			"locationId" : "fc73deca-0eef-4538-8759-b0bf230e88cc", 
			"active" : true
		},
		{
			"locationId" : "fa67f14a-2396-41c3-af08-0cb0d3e6d55b", 
			"active" : true
		},
		{
			"locationId" : "46fc799d-f254-453a-b40b-bb2fb98a49f1", 
			"active" : true
		}
		],
	"mediaAvailable" : false
	
}
```

**2. Get coupon by ID** <br/>
`GET /coupon/{id}`

```
{
  "name": "couponName",
  "description": "coupon description",
  "scopes": [
    {
      "locationId": "3cf9ac98-467f-45dd-b7ef-e40143f51070",
      "active": true
    }
  ],
  "mediaAvailable": false,
  "active": true,
  "id": "87a8354b-ed47-4e4b-94c7-4ca5393e9c0c",
  "code": "aWNhcnVzfDFiMWQ3NGI3NWMyYmY3NzZlN2JiNWZhMjA5NDExNTE3Y2E4MTkyNjUyZTk3MTJjZGJjZWJlN2QzMzJhYWI5Mzc="
}

```

**3. Get coupon by Code**<br/>
`GET /coupon/code/{code}`

**4. Get all tenant coupons**<br/>
`GET /coupon/all`

**5. Remove all tenant coupons**<br/>
`DELETE /coupon/all`

**6. Add scopes for coupon**<br/>
`PATCH /coupon/{id}/scopes`
```
[
		{
			"locationId" : "11111111-498e-4c53-9705-76a158dd2d85", 
			"active" : true
		}
]
```

**7. Remove scopes for coupon**<br/>
`DELETE /coupon/{id}/scopes`
```
[
    "11111111-498e-4c53-9705-76a158dd2d85"
]
```

**8. Add scopes for all coupons of tenant**<br/>
`PATCH /coupon/all/scopes`
```
[
		{
			"locationId" : "11111111-498e-4c53-9705-76a158dd2d85", 
			"active" : true
		}
]
```

**9. Remove scopes for all coupons of tenant**<br/>
`DELETE /coupon/all/scopes`
```
[
    "11111111-498e-4c53-9705-76a158dd2d85"
]
```

**10. Enable coupon globally**<br/>
`PUT /coupon/{id}/active`

**11. Disable coupon globally**<br/>
`DELETE /coupon/{id}/active`

**12. Enable all tenant's coupons globally**<br/>
`PUT /coupon/all/active`

**13. Disable all tenant's coupons globally**<br/>
`DELETE /coupon/all/active`

**14. Enable coupon in specified location**<br/>
`PUT /coupon/{id}/active/location/{locationId}`

**15. Disable coupon in specified location**<br/>
`DELETE /coupon/{id}/active/location/{locationId}`

**16. Enable tenant coupons in specified locations**<br/>
`PUT /coupon/all/active/location/{locationId}`

**17. Disable tenant coupons in specified location**<br/>
`DELETE /coupon/all/active/location/{locationId}`